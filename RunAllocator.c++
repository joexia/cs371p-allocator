// ---------------------------------------
// projects/c++/allocator/RunAllocator.c++
// Copyright (C) 2018
// Glenn P. Downing
// ---------------------------------------

// --------
// includes
// --------

#include <cassert>  // assert
#include <fstream>  // ifstream
#include <iostream> // cout, endl, getline
#include <string>   // s

#include "Allocator.h"

// ----
// main
// ----

int main() {
    /* Enter your code here. Read input from STDIN. Print output to STDOUT */

    //get number of tests
    using namespace std;
    string s;
    getline(cin, s);
    int cases = atoi(s.c_str());
    assert(cases < 100);

    //start parsing through the input
    getline(cin, s);
    for(int i = 0; i < cases; ++i) {
        my_allocator<double, 1000> a;
        getline(cin, s);
        do {
            //if it's positive, allocate
            int input = atoi(s.c_str());
            if(input > 0) {
                a.allocate(input);
            }

            //if negative, find pointer to ith taken block and deallocate
            if(input < 0) {
                my_allocator<double, 1000>::iterator find = (char*)&a[0];
                int found = 0;
                int pos = 0;
                int last = 0;
                while(found > input && *find != 0) {
                    if(*find < 0) {
                        --found;
                    }
                    pos += last;
                    int cur = *find;
                    if(cur < 0) {
                        cur = cur * -1;
                    }
                    last = cur + (2*sizeof(int));
                    ++find;
                }
                a.deallocate((double*)&a[pos + sizeof(int)], 0);
            }
        } while(s != "" && getline(cin, s));

        //print the resulting array
        my_allocator<double, 1000>::iterator b = (char*)&a[0];
        while(*b != 0) {
            cout<<*b;
            ++b;
            if(*b != 0) {
                cout << " ";
            }
        }
        cout<<endl;
    }

    return 0;
}
