// ----------------------------------------
// projects/c++/allocator/TestAllocator.c++
// Copyright (C) 2018
// Glenn P. Downing
// ----------------------------------------

// https://github.com/google/googletest
// https://github.com/google/googletest/blob/master/googletest/docs/primer.md
// https://github.com/google/googletest/blob/master/googletest/docs/advanced.md

// --------
// includes
// --------

#include <algorithm> // count
#include <memory>    // allocator

#include "gtest/gtest.h"

#include "Allocator.h"

// --------------
// TestAllocator1
// --------------

template <typename A>
struct TestAllocator1 : testing::Test {
    // --------
    // typedefs
    // --------

    typedef          A             allocator_type;
    typedef typename A::value_type value_type;
    typedef typename A::size_type  size_type;
    typedef typename A::pointer    pointer;
};

typedef testing::Types<
std::allocator<int>,
    std::allocator<double>,
    my_allocator<int,    100>,
    my_allocator<double, 100>>
    my_types_1;

TYPED_TEST_CASE(TestAllocator1, my_types_1);

TYPED_TEST(TestAllocator1, test_1) {
    typedef typename TestFixture::allocator_type allocator_type;
    typedef typename TestFixture::value_type     value_type;
    typedef typename TestFixture::size_type      size_type;
    typedef typename TestFixture::pointer        pointer;

    allocator_type x;
    const size_type  s = 1;
    const value_type v = 2;
    const pointer    p = x.allocate(s);
    if (p != nullptr) {
        x.construct(p, v);
        ASSERT_EQ(v, *p);
        x.destroy(p);
        x.deallocate(p, s);
    }
}

TYPED_TEST(TestAllocator1, test_10) {
    typedef typename TestFixture::allocator_type allocator_type;
    typedef typename TestFixture::value_type     value_type;
    typedef typename TestFixture::size_type      size_type;
    typedef typename TestFixture::pointer        pointer;

    allocator_type   x;
    const size_type  s = 10;
    const value_type v = 2;
    const pointer    b = x.allocate(s);
    if (b != nullptr) {
        pointer e = b + s;
        pointer p = b;
        try {
            while (p != e) {
                x.construct(p, v);
                ++p;
            }
        }
        catch (...) {
            while (b != p) {
                --p;
                x.destroy(p);
            }
            x.deallocate(b, s);
            throw;
        }
        ASSERT_EQ(s, std::count(b, e, v));
        while (b != e) {
            --e;
            x.destroy(e);
        }
        x.deallocate(b, s);
    }
}

// --------------
// TestAllocator2
// --------------

TEST(TestAllocator2, const_index) {
    const my_allocator<int, 100> x;
    ASSERT_EQ(x[0], 92);
}

TEST(TestAllocator2, index) {
    my_allocator<int, 100> x;
    ASSERT_EQ(x[0], 92);
}

TEST(TestAllocator2, index2) {
    my_allocator<int, 100> x;
    x.allocate(5);
    ASSERT_EQ(x[0], -20);
    ASSERT_EQ(x[24], -20);
    ASSERT_EQ(x[28], 64);
    ASSERT_EQ(x[96], 64);
    x.allocate(16);
    ASSERT_EQ(x[0], -20);
    ASSERT_EQ(x[24], -20);
    ASSERT_EQ(x[28], -64);
    ASSERT_EQ(x[96], -64);
}

TEST(TestAllocator2, index3) {
    my_allocator<int, 100> x;
    x.allocate(23);
    ASSERT_EQ(x[0], -92);
}

TEST(TestAllocator2, index4) {
    my_allocator<int, 100> x;
    x[0] = 4;
    x[8] = 4;
    x[12] = 80;
    x[96] = 80;
    bool check = TestValid(x);
    ASSERT_EQ(false, check);
    x[8] = 15;
    check = TestValid(x);
    ASSERT_EQ(false, check);
}

TEST(TestAllocator2, index5) {
    my_allocator<int, 100> x;
    try {
        x.deallocate(0, 0);
    } catch(invalid_argument) {
        ASSERT_EQ(x[0], 92);
    }
}

// --------------
// TestAllocator3
// --------------

template <typename A>
struct TestAllocator3 : testing::Test {
    // --------
    // typedefs
    // --------

    typedef          A             allocator_type;
    typedef typename A::value_type value_type;
    typedef typename A::size_type  size_type;
    typedef typename A::pointer    pointer;
};

typedef testing::Types<
my_allocator<int,    100>,
             my_allocator<double, 100>>
             my_types_2;

TYPED_TEST_CASE(TestAllocator3, my_types_2);

TYPED_TEST(TestAllocator3, test_1) {
    typedef typename TestFixture::allocator_type allocator_type;
    typedef typename TestFixture::value_type     value_type;
    typedef typename TestFixture::size_type      size_type;
    typedef typename TestFixture::pointer        pointer;

    allocator_type   x;
    const size_type  s = 1;
    const value_type v = 2;
    const pointer    p = x.allocate(s);
    if (p != nullptr) {
        x.construct(p, v);
        ASSERT_EQ(v, *p);
        x.destroy(p);
        x.deallocate(p, s);
    }
}

TYPED_TEST(TestAllocator3, test_2) {
    typedef typename TestFixture::allocator_type allocator_type;
    typedef typename TestFixture::value_type     value_type;
    typedef typename TestFixture::size_type      size_type;
    typedef typename TestFixture::pointer        pointer;

    allocator_type   x;
    const size_type  s = 1400;
    const value_type v = 2;
    try {
        const pointer    p = x.allocate(s);
    } catch (bad_alloc) {
        ASSERT_EQ(x[0], 92);
    }
    try {
        const pointer    p = x.allocate(0);
    } catch (bad_alloc) {
        ASSERT_EQ(x[0], 92);
    }

    const pointer p1 = x.allocate(1);
    const pointer p2 = x.allocate(1);
    const pointer p3 = x.allocate(1);
    x.deallocate(p1, s);
    x.deallocate(p3, s);
    x.deallocate(p2, s);

    try {
        x.deallocate(p3, s);
    } catch(invalid_argument) {
        ASSERT_EQ(x[0], 92);
    }

    ASSERT_EQ(x[0], 92);

}
