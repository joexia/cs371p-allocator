// ----------------------------------
// projects/c++/allocator/Allocator.h
// Copyright (C) 2018
// Glenn P. Downing
// ----------------------------------

#ifndef Allocator_h
#define Allocator_h

// --------
// includes
// --------

#include <cassert>   // assert
#include <cstddef>   // ptrdiff_t, size_t
#include <new>       // bad_alloc, new
#include <stdexcept> // invalid_argument

// ---------
// Allocator
// ---------

using namespace std;

template <typename T, std::size_t N>
class my_allocator {

    //used to test valid function
    friend bool TestValid(my_allocator &);

    // -----------
    // operator ==
    // -----------

    friend bool operator == (const my_allocator&, const my_allocator&) {
        return false;
    }                                                   // this is correct

    // -----------
    // operator !=
    // -----------

    friend bool operator != (const my_allocator& lhs, const my_allocator& rhs) {
        return !(lhs == rhs);
    }

public:
    // --------
    // typedefs
    // --------

    using      value_type = T;

    using       size_type = std::size_t;
    using difference_type = std::ptrdiff_t;

    using       pointer   =       value_type*;
    using const_pointer   = const value_type*;

    using       reference =       value_type&;
    using const_reference = const value_type&;

private:
    // ----
    // data
    // ----

    char a[N];

    // -----
    // valid
    // -----

    /**
     * O(1) in space
     * O(n) in time
     * checks that the sentenals are the same
     * on both ends, and then makes sure there
     * aren't any adjacent free blocks. not using
     * the iterator i made because it gave me
     * weird errors
     */
    bool valid () const {
        //while we haven't hit the end of a
        int pos = 0;
        while(pos < N) {
            //get the front
            int* front = (int*)&a[pos];
            int abs = *front;

            //get the back sentanal
            if(abs < 0) {
                abs = abs * -1;
            }

            //if they're not the same, return false;
            int* back = (int*)&a[pos + abs + sizeof(int)];
            if(*front != *back) {
                return false;
            }

            //iterate to next block
            pos += abs + sizeof(int) * 2;

            //make sure at least one is not free, return false
            //if both are
            if(pos < N) {
                int* next = (int*)&a[pos];
                if(*next > 0 && *front > 0) {
                    return false;
                }
            }
        }
        return true;
    }

public:
    // --------
    // iterator
    // --------

    class iterator {
        // -----------
        // operator ==
        // -----------

        friend bool operator == (const iterator& lhs, const iterator& rhs) {
            return *lhs == *rhs;
        }

        // -----------
        // operator !=
        // -----------

        friend bool operator != (const iterator& lhs, const iterator& rhs) {
            return !(lhs == rhs);
        }

    private:
        // ----
        // data
        // ----

        char* _p;
        //used to keep track and make sure we're not
        //going past the end of the array
        int _check;

    public:
        // -----------
        // constructor
        // -----------

        iterator (char* p) {
            _p = p;
            _check = 0;
        }

        // ----------
        // operator *
        // ----------

        //I'm definately not using this iterator as I was meant to use it
        //but...
        int operator * () const {
            //return 0 if we've gone past the end of the array
            if(_check == N) {
                return 0;
            }

            //else just return the value of the sentanal
            int* int_p = (int*)_p;
            return *int_p;
        }

        // -----------
        // operator ++
        // -----------

        iterator& operator ++ () {
            //make sure that we haven't hit the
            //end of the array
            if(_check != N) {
                int* next = (int*)_p;
                int n = *next;
                //make sure that we use the abs value
                if(n < 0) {
                    n = n * -1;
                }

                //iterate through
                _p += n + (2*sizeof(int));
                _check += n + (2*sizeof(int));
            }
            return *this;
        }

        // -----------
        // operator ++
        // -----------

        iterator operator ++ (int) {
            iterator x = *this;
            ++*this;
            return x;
        }
    };

    // -----------
    // constructor
    // -----------

    /**
     * O(1) in space
     * O(1) in time
     * throw a bad_alloc exception, if N is less than sizeof(T) + (2 * sizeof(int))
     */
    my_allocator () {
        //assert preconditions (it says N must be < 1000 bytes but then
        //we use 1000 bytes for almost all of our tests so...)
        assert(N <= 1000 && N > 0);

        //meet the documentation standards
        if(N < sizeof(T) + (2*sizeof(int))) {
            throw std::bad_alloc();
        }

        //create the initial block
        *reinterpret_cast<int*>(&a[0]) = N - (2 * sizeof(int));
        *reinterpret_cast<int*>(&a[N - sizeof(int)]) = N - (2 * sizeof(int));

        assert(valid());
    }

    my_allocator             (const my_allocator&) = default;
    ~my_allocator            ()                    = default;
    my_allocator& operator = (const my_allocator&) = default;

    // --------
    // allocate
    // --------

    /**
     * O(1) in space
     * O(n) in time
     * after allocation there must be enough space left for a valid block
     * the smallest allowable block is sizeof(T) + (2 * sizeof(int))
     * choose the first block that fits
     * throw a bad_alloc exception, if n is invalid
     */
    pointer allocate (size_type s) {
        //if trying to allocate something 0 or less than 0, throw exception
        if(s <= 0) {
            throw std::bad_alloc();
        }

        //start going through the free blocks to find one big enough
        my_allocator<T, N>::iterator i = (char*)&a[0];
        int to_allocate = s*sizeof(T);
        int block_loc = 0;

        //keep track of the pointer to the block, when find one big enough
        //exit loop
        while(*i < (to_allocate) && *i != 0) {
            int add = *i;
            if(add < 0) {
                add = add * -1;
            }
            add += 2 * sizeof(int);
            block_loc += add;
            ++i;
        }

        //if no block big enough found, throw exception
        //also covers in case they give us a too big block
        if(*i == 0) {
            throw std::bad_alloc();
        }


        //calculate if we have enough space to make a new block
        int newsize = (*i - to_allocate - (2*sizeof(int)));
        int oldsize = *i;

        //if not, then just return the whole block
        int check = sizeof(T);
        if(newsize < check) {
            *reinterpret_cast<int*>(&a[block_loc]) = -1 * oldsize;
            *reinterpret_cast<int*>(&a[block_loc + oldsize + sizeof(int)]) = -1 * oldsize;
            assert(valid());
            return reinterpret_cast<pointer>(&a[block_loc + sizeof(int)]);
        }

        //create the two new blocks
        *reinterpret_cast<int*>(&a[block_loc]) = -1 * to_allocate;
        *reinterpret_cast<int*>(&a[block_loc + to_allocate + sizeof(int)]) = -1 * to_allocate;
        *reinterpret_cast<int*>(&a[block_loc + to_allocate + (2 * sizeof(int))]) = newsize;
        *reinterpret_cast<int*>(&a[block_loc + oldsize + sizeof(int)]) = newsize;

        assert(valid());
        return reinterpret_cast<pointer>(&a[block_loc + sizeof(int)]);
    }

    // ---------
    // construct
    // ---------

    /**
     * O(1) in space
     * O(1) in time
     */
    void construct (pointer p, const_reference v) {
        new (p) T(v);                               // this is correct and exempt
        assert(valid());
    }                           // from the prohibition of new

    // ----------
    // deallocate
    // ----------

    /**
     * O(1) in space
     * O(1) in time
     * after deallocation adjacent free blocks must be coalesced
     * throw an invalid_argument exception, if p is invalid
     * Takes in a pointer, verifies its validity, sets its sentinals,
     * and merges with adjacent open blocks.
     */
    void deallocate (pointer p, size_type) {

        //if pointer is not within range of array, throw exception
        if(p < (pointer)&a[0] || p >= (pointer)&a[N-1]) {
            throw std::invalid_argument("invalid pointer");
        }

        //finds and sets the begin and end sentinals of the memory
        pointer begin = (pointer)((char*)p - sizeof(int));
        int* beginsent = reinterpret_cast<int*>(begin);
        *beginsent = *beginsent * -1;
        pointer end = (pointer)((char*)p + *beginsent);
        int* endsent = reinterpret_cast<int*>(end);
        *endsent = *endsent * -1;

        //if sentinals don't match, throw exception
        if(*beginsent != *endsent) {
            throw std::invalid_argument("invalid pointer");
        }

        //find the bottom sentinal of the block above it
        pointer btop = (pointer)((char*)begin - sizeof(int));

        //make sure its not end of the array
        if(btop >= (pointer)&a[0]) {

            //find the top of the block above it
            int* btopsent = reinterpret_cast<int*>(btop);

            //merge them if free
            if(*btopsent > 0) {
                pointer bbot = (pointer)((char*)btop - *btopsent - sizeof(int));
                int* bbotsent = reinterpret_cast<int*>(bbot);
                int newspace = *bbotsent + *beginsent + (2 * sizeof(int));
                *endsent = newspace;
                *bbotsent = newspace;
                begin = bbot;
                beginsent = reinterpret_cast<int*>(begin);
            }
        }

        //find block underneath it
        pointer tbot = (pointer)((char*)end + sizeof(int));

        //make sure not the bottom of the array
        if(tbot < (pointer)&a[N - 1]) {

            //find the top of the block beneath it
            int* tbotsent = reinterpret_cast<int*>(tbot);

            //if it's free, merge them
            if(*tbotsent > 0) {
                pointer ttop = (pointer)((char*)tbot + *tbotsent + sizeof(int));
                int* ttopsent = reinterpret_cast<int*>(ttop);
                int newspace = *ttopsent + *beginsent + (2 * sizeof(int));
                *ttopsent = newspace;
                *beginsent = newspace;
            }
        }

        assert(valid());
    }

    // -------
    // destroy
    // -------

    /**
     * O(1) in space
     * O(1) in time
     */
    void destroy (pointer p) {
        p->~T();               // this is correct
        assert(valid());
    }

    // -----------
    // operator []
    // -----------

    /**
     * O(1) in space
     * O(1) in time
     */
    int& operator [] (int i) {
        return *reinterpret_cast<int*>(&a[i]);
    }

    // -----------
    // operator []
    // -----------

    /**
     * O(1) in space
     * O(1) in time
     */
    const int& operator [] (int i) const {
        return *reinterpret_cast<const int*>(&a[i]);
    }
};

//friend method used for testing valid
bool TestValid(my_allocator<int, 100> &x) {
    return x.valid();
}

#endif // Allocator_h
